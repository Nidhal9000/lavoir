package com.baeldung.ecommerce.controller;

import com.baeldung.ecommerce.model.PressingDomicileCommande;
import com.baeldung.ecommerce.model.Product;
import com.baeldung.ecommerce.service.PressingDomicileCommandeService;
import com.baeldung.ecommerce.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    private PressingDomicileCommandeService pressingDomicileCommandeService;

    private ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("all")
    public @NotNull
    Iterable<Product> getProducts() {
        return productService.getAllProducts();
    }

    @PostMapping("/create")
    public Product createProduct(@RequestBody Product product) {
        return productService.save(product);
    }

    @PostMapping("/pressdomicile")
    public PressingDomicileCommande createPressingDomicileCommande(@RequestBody PressingDomicileCommande pressingDomicileCommande) {
        return pressingDomicileCommandeService.save(pressingDomicileCommande);
    }

    @GetMapping("/pressdomicile/all")
    public Iterable<PressingDomicileCommande> getAllPressCommande(){
        return pressingDomicileCommandeService.getAllPressingDomicileCommande();
    }
}
