package com.baeldung.ecommerce.controller;

import com.baeldung.ecommerce.model.User;
import com.baeldung.ecommerce.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/inscription")
    public User createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @PostMapping("/login")
    public User login(@RequestBody User user) {
        User userTmp = userService.getUserByLogin(user.getLogin());
        if (userTmp != null && userTmp.getPassword().equals(user.getPassword())) {
            return userTmp;
        } else {
            return null;
        }
    }
}
