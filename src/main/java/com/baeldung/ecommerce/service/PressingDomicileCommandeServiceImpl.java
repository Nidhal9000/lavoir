package com.baeldung.ecommerce.service;

import com.baeldung.ecommerce.model.PressingDomicileCommande;
import com.baeldung.ecommerce.repository.PressingDomicileCommandeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PressingDomicileCommandeServiceImpl implements PressingDomicileCommandeService {

    @Autowired
    private PressingDomicileCommandeRepository pressingDomicileCommandeRepository;

    @Override
    public Iterable<PressingDomicileCommande> getAllPressingDomicileCommande() {
        return pressingDomicileCommandeRepository.findAll();
    }

    @Override
    public PressingDomicileCommande getPressingDomicileCommande(long id) {
        return pressingDomicileCommandeRepository.findOne(id);
    }

    @Override
    public PressingDomicileCommande save(PressingDomicileCommande pressingDomicileCommande) {
        return pressingDomicileCommandeRepository.save(pressingDomicileCommande);
    }
}
