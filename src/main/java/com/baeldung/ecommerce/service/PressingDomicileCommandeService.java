package com.baeldung.ecommerce.service;

import com.baeldung.ecommerce.model.PressingDomicileCommande;
import org.springframework.validation.annotation.Validated;

@Validated
public interface PressingDomicileCommandeService {

    Iterable<PressingDomicileCommande> getAllPressingDomicileCommande();

    PressingDomicileCommande getPressingDomicileCommande(long id);

    PressingDomicileCommande save(PressingDomicileCommande pressingDomicileCommande);
}
