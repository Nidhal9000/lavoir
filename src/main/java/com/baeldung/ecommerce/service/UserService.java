package com.baeldung.ecommerce.service;

import com.baeldung.ecommerce.model.User;
import com.baeldung.ecommerce.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User getUserByLogin(String login) {
        return userRepository.findUserByLogin(login);
    }

    public User createUser(User user) {
        return userRepository.save(user);
    }
}
