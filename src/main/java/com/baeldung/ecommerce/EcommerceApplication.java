package com.baeldung.ecommerce;

import com.baeldung.ecommerce.model.Product;
import com.baeldung.ecommerce.model.User;
import com.baeldung.ecommerce.service.ProductService;
import com.baeldung.ecommerce.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class EcommerceApplication {

    @Autowired
    private UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(EcommerceApplication.class, args);
    }

    @Bean
    CommandLineRunner runner(ProductService productService) {
        return args -> {
            productService.save(new Product( "Brosse", 300.00, "/assets/images/brs01.jpg",""));
            productService.save(new Product( "Parfum desinfectant", 200.00, "https://image.noelshack.com/fichiers/2019/19/6/1557585758-brosse.jpg",""));
            productService.save(new Product( "Deboulocheur", 100.00, "http://placehold.it/200x100",""));
            productService.save(new Product("Icecream", 5.00, "http://placehold.it/200x100",""));
            productService.save(new Product("Beer", 3.00, "http://placehold.it/200x100",""));
            productService.save(new Product( "Phone", 500.00, "http://placehold.it/200x100",""));
            productService.save(new Product( "Watch", 30.00, "http://placehold.it/200x100",""));

            User admin = new User();
            admin.setRole("ADMIN");
            admin.setLogin("admin");
            admin.setPassword("changeme");
            admin.setFirsName("admin");
            userService.createUser(admin);
        };
    }
}
