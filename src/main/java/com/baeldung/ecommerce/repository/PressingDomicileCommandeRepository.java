package com.baeldung.ecommerce.repository;

import org.springframework.data.repository.CrudRepository;

public interface PressingDomicileCommandeRepository extends CrudRepository<com.baeldung.ecommerce.model.PressingDomicileCommande, Long> {
}
